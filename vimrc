call plug#begin('~/.vim/plugged')

Plug 'pangloss/vim-javascript'
Plug 'othree/html5.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'bronson/vim-trailing-whitespace'
Plug 'chase/vim-ansible-yaml'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'rking/ag.vim'
Plug 'scrooloose/nerdcommenter'
"Plug 'scrooloose/nerdtree'
"Plug 'scrooloose/syntastic'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'

call plug#end()

syntax enable

set background=dark
set backspace=2
set expandtab
set hidden
set hlsearch
set laststatus=2
set nowrap
set nu
set shell=/bin/sh
set shiftwidth=4
set sts=4
set t_Co=256
set tabstop=4
set wildignore+=*.pyc,*.sqlite,*.dump,*.lst,*.gif,*.png,*.jpg,*node_modules/*,*egg-info/*,*.sqlite3,*.class

autocmd FileType css setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType htmldjango setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType python setlocal ts=4 sts=4 sw=4 expandtab

let mapleader = ' '

map <F2> :Explore<CR>
map <F3> :changes<CR>
map <F4> :jumps<CR>
map <Leader><Space> :FZF<CR>
map <Leader>b :Buffers<CR>
nnoremap <Leader>M <C-w>_<C-w>\|
nnoremap <Leader>n :noh<CR>
nnoremap <Leader>o :only<CR>
nnoremap <Leader>w <C-w>w
vmap <C-c> y:call system("pbcopy", getreg("\""))<CR>

" highlight column 100
if (exists('+colorcolumn'))
    set colorcolumn=80
    highlight ColorColumn ctermbg=9
endif

" Plugin configuration

" colorscheme - solarized
let g:solarized_termcolors=256
colorscheme gruvbox

" airline - statusline
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='gruvbox'

" use powerline symbols
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline#extensions#tabline#left_sep = ''
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" nerdtree
let NERDTreeIgnore = ['\.pyc$']

" syntastic
let g:syntastic_python_flake8_args = '--ignore=E501,E128,E265,F403,E262,E113,E124'

" close nerdtree when its last
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

set rtp+=/usr/local/opt/fzf
