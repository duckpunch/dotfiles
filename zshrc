[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export PATH=/Users/frank/bin:/Users/frank/code/webdrivers:$PATH
export LSCOLORS=Gxfxcxdxbxegedabagacad

alias ls="ls -G"
alias tree="tree -I \"*.pyc|node_modules|*.egg-info|__pycache__\""
export FZF_DEFAULT_COMMAND='ag -f --ignore "*.pyc" -g ""'

setopt prompt_subst # enable command substition in prompt

autoload -U colors && colors

PROMPT='%{$fg[cyan]%}$(date +"[%I:%M:%S]")%{$fg[green]%} $(whoami)@$(hostname -s):%{$fg[blue]%}$(rel_dir)/ %{$reset_color%}' # single quotes to prevent immediate execution
RPROMPT='' # no initial prompt, set dynamically

ASYNC_PROC=0
function precmd() {
    function async() {
        # save to temp file
        printf "%s" "%{$fg[cyan]%}$(vcprompt -f "[%n:%b:%m%u] ")%{$reset_color%}" > "/tmp/zsh_prompt_$$"
        # signal parent
        kill -s USR1 $$
    }

    # do not clear RPROMPT, let it persist

    # kill child if necessary
    if [[ "${ASYNC_PROC}" != 0 ]]; then
        kill -s HUP $ASYNC_PROC >/dev/null 2>&1 || :
    fi

    # start background computation
    async &!
    ASYNC_PROC=$!
}

function TRAPUSR1() {
    # read from temp file
    RPROMPT="$(cat /tmp/zsh_prompt_$$)"

    # reset proc number
    ASYNC_PROC=0

    # redisplay
    zle && zle reset-prompt
}

function rel_dir() {
    local dir="$(pwd)"
    local relative="${dir/$HOME/~}"
    echo $relative | sed -e "s/\(.\)[^\/]*\//\1\//g"
}

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history
setopt inc_append_history
setopt share_history
setopt extended_history

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

#source /usr/local/bin/virtualenvwrapper.sh

if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval $(ssh-agent)
fi

alias ssh-add-self="ssh-add ~/.ssh/keys/self"
export PATH="/usr/local/sbin:$PATH"
export PYSPARK_PYTHON=python3
